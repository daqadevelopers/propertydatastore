<?php

#required for Africaistalking API
//use AfricasTalking\SDK\AfricasTalking;
#classes for the first API
// use infobip\api\client\SendMultipleTextualSmsAdvanced;
// use infobip\api\configuration\BasicAuthConfiguration;
// use infobip\api\model\Destination;
// use infobip\api\model\sms\mt\send\Message;
// use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
if (!defined('BASEPATH'))
    exit("No direct script access allowed");

class Helpers {

    protected $CI;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->CI->load->model('user_model', '', TRUE);
    }

       function log_online_status_create($id){
        $data = array('TIME'=>time(),
                    'STATUS'=>1,
                    'LASTSEEN'=>time()
                );
        $this->CI->user_model->update_session($id, $data);
       }

       

    public function dynamic_script_tags($js = false, $css = false) {

        $requiredjs = "";
        $requiredcss = "";
        $assets_folder = "assets/";
        if (!($js === false)) {
            foreach ($js as $key => $link) {
                $requiredjs .= $this->CI->template->javascript->add(base_url($assets_folder . "js/" . $link));
            }
        }
        if (!($css === false)) {
            foreach ($css as $key => $link) {
                $requiredcss .= $this->CI->template->stylesheet->add(base_url($assets_folder . "css/" . $link), array('media' => 'all'));
            }
        }

        return json_encode($requiredjs . '' . $requiredcss);
    }

    public function yr_transformer($form_date) {
        $exploded_date = explode('-', $form_date, 3);
        $new_date = count($exploded_date) === 3 ? ($exploded_date[2] . "-" . $exploded_date[1] . "-" . $exploded_date[0]) : null;
        return preg_replace("/^19/", "20", $new_date);
    }


    public function upload_file($location, $max_size = 1024, $allowed_types = "gif|jpg|jpeg|png|pdf") {
        $config['upload_path'] = APPPATH . "../uploads/$location/";
        $config['allowed_types'] = $allowed_types;
        $config['max_size'] = $max_size;
        $config['max_filename'] = 120;
        $config['overwrite'] = true;
        $config['file_ext_tolower'] = true;
        $config['file_name'] = $_FILES['file_attachment']['name'];

        //$upload_feedback = [];
        $this->CI->load->library('upload', $config);
        //if the folder doesn't exist
        if (!is_dir($config["upload_path"])) {
            mkdir($config["upload_path"], 0777, true);
        }
        if (!$this->CI->upload->do_upload('file_attachment')) {
            //$upload_feedback['error'] = array('error' => $this->upload->display_errors());
            //return false;
        } else {
            return $this->CI->upload->data('file_name');
        }
    }
    public function send_email($send_to_list,$subject,$message){
        $body='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>System Auto -EMAILS</title>
   <style>
   /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
}

img {
    max-width: 100%;
}

body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6;
}

table td {
    vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
    background-color: #f6f6f6;
}

.body-wrap {
    background-color: #f6f6f6;
    width: 100%;
}

.container {
    display: block !important;
    max-width: 600px !important;
    margin: 0 auto !important;
    /* makes it centered */
    clear: both !important;
}

.content {
    max-width: 600px;
    margin: 0 auto;
    display: block;
    padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
    background: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
}

.content-wrap {
    padding: 20px;
}

.content-block {
    padding: 0 0 20px;
}

.header {
    width: 100%;
    margin-bottom: 20px;
}

.footer {
    width: 100%;
    clear: both;
    color: #999;
    padding: 20px;
}
.footer a {
    color: #999;
}
.footer p, .footer a, .footer unsubscribe, .footer td {
    font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    color: #000;
    margin: 30px 0 0;
    line-height: 1.2;
    font-weight: 400;
}

h1 {
    font-size: 52px;
    font-weight: bold;
	margin-bottom: 40px;
	color:white;
}

h2 {
    font-size: 24px;
}

h3 {
    font-size: 18px;
}

h4 {
    font-size: 14px;
    font-weight: 600;
}

p, ul, ol {
    margin-bottom: 10px;
    font-weight: normal;
}
p li, ul li, ol li {
    margin-left: 5px;
    list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
    color: #1ab394;
    text-decoration: underline;
}

.btn-primary {
    text-decoration: none;
    color: #FFF;
    background-color: #1ab394;
    border: solid #1ab394;
    border-width: 5px 10px;
    line-height: 2;
    font-weight: bold;
    text-align: center;
    cursor: pointer;
    display: inline-block;
    border-radius: 5px;
    text-transform: capitalize;
}
.btn-info {
    text-decoration: none;
    color: #FFF;
    background-color: #17a2b8;
    border: solid #17a2b8;
    border-width: 5px 10px;
    line-height: 2;
    font-weight: bold;
    text-align: center;
    cursor: pointer;
    display: inline-block;
    border-radius: 5px;
    text-transform: capitalize;
}


/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
    margin-bottom: 0;
}

.first {
    margin-top: 0;
}

.aligncenter {
    text-align: center;
}

.alignright {
    text-align: right;
}

.alignleft {
    text-align: left;
}

.clear {
    clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
    font-size: 16px;
    color: #fff;
    font-weight: 500;
    padding: 20px;
    text-align: center;
    border-radius: 3px 3px 0 0;
}
.alert a {
    color: #fff;
    text-decoration: none;
    font-weight: 500;
    font-size: 16px;
}
.alert.alert-warning {
    background: #f8ac59;
}
.alert.alert-bad {
    background: #ed5565;
}
.alert.alert-good {
    background: #1ab394;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
    margin: 40px auto;
    text-align: left;
    width: 80%;
}
.invoice td {
    padding: 5px 0;
}
.invoice .invoice-items {
    width: 100%;
}
.invoice .invoice-items td {
    border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
    border-top: 2px solid #333;
    border-bottom: 2px solid #333;
    font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {
    h1, h2, h3, h4 {
        font-weight: 600 !important;
        margin: 20px 0 5px !important;
    }

    h1 {
        font-size: 22px !important;
    }

    h2 {
        font-size: 18px !important;
    }

    h3 {
        font-size: 16px !important;
    }

    .container {
        width: 100% !important;
    }

    .content, .content-wrap {
        padding: 10px !important;
    }

    .invoice {
        width: 100% !important;
    }
}

   </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="100%">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="1" cellspacing="1">
                                '.$message.'
                            </table>
                        </td>
                    </tr>
                </table>
              </div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>';
$this->CI->load->library('email');
        $this->CI->email->from('forisu.1000@gmail.com', 'DotCom Values');
        $this->CI->email->to($send_to_list);
        $this->CI->email->reply_to('ajuna101@gmail.com', 'System Admin');
        $this->CI->email->subject($subject);
        $this->CI->email->message($body);
        if($this->CI->email->send()){
            return true;
        }else{
            return false;
        }
    }    

}
