<?php

class Property_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_distinct($filter = FALSE) { //per district
        $sub_query= '';
        if ($filter === false) {
           $sub_query= '';
        }else{
            $sub_query= ' WHERE '.$filter.' ';
        }
        $query = $this->db->query('SELECT * from(SELECT DISTRICT_ID, count(*) as count from property group by DISTRICT_ID) d1 left join district on DISTRICT_ID=district.id ORDER BY d1.count desc limit 15' );
       
         return $query->result_array();
    }
    public function get_per_entrant($filter) {  //prop per user
        $query = $this->db->query('SELECT users.id,users.SALUTATION,users.FIRSTNAME,users.INITIALS,users.PHOTO,users.LASTNAME,users.OTHERNAMES,d1.CREATED_BY,d1.DATE_CREATED,d1.count from(SELECT property.CREATED_BY,property.DATE_CREATED, count(id) as count from property '.$filter.' group by property.CREATED_BY) d1 left join users on d1.CREATED_BY=users.id');
       //print_r($this->db->last_query()); die;
         return $query->result_array();
    }

    public function get_per_user($filter = FALSE) {  //prop per user
        $sub_query= '';
        if ($filter === false) {
           $sub_query= '';
        }else{
            $sub_query= ' WHERE '.$filter.' ';
        }
        $query = $this->db->query('SELECT users.SALUTATION,users.FIRSTNAME,users.INITIALS,users.PHOTO,users.LASTNAME,users.OTHERNAMES,d1.VALUER_ID,d1.DATE_OF_VAL,d1.DATE_CREATED,d1.count from(SELECT property.VALUER_ID,property.DATE_OF_VAL,property.DATE_CREATED, count(*) as count from property group by VALUER_ID) d1 left join users on VALUER_ID=users.id ORDER BY  d1.count DESC');
       
         return $query->result_array();
    }
    public function get_per_month($filter = FALSE) {  //prop per user
       
        $query = $this->db->query('SELECT d1.month,d1.count from(SELECT MONTH(DATE_OF_VAL) as month, count(*) as count from property WHERE YEAR(DATE_OF_VAL)='.$filter.' group by MONTH(DATE_OF_VAL)) d1');
       
         return $query->result_array();
    }
    public function get($filter = FALSE) {

        $this->db->select('p.id,TENURE,PROPERTY_ADDRESS,NORTH,EAST,DATE_OF_VAL,ACREAGE,RATE_PER_ACRE,PROPERTY_VALUE,SERIAL_NO,BANK_OPTION,USER_STATUS,USER_OPTION,p.NOTES,TOWN_ID,VILLAGE_ID,p.DATE_CREATED,p.ZONE,p.CONVEAST,p.CONVNORTH,p.category_id,p.DATE_MODIFIED,uv.SALUTATION as valuer_salutation,uv.FIRSTNAME as valuer_firstname,uv.LASTNAME as valuer_lastname,uv.OTHERNAMES as valuer_othernames,uv.INITIALS as valuer_initials,uc.SALUTATION as created_salutation,uc.FIRSTNAME as created_firstname,uc.LASTNAME as created_lastname,uc.OTHERNAMES as created_othernames,um.SALUTATION as modified_salutation,um.FIRSTNAME as modified_firstname,um.LASTNAME as modified_lastname,um.OTHERNAMES as modified_othernames,BANK_ID,b.BANK_NAME,DISTRICT_ID,d.DISTRICT_NAME,p.FIRM_ID,FIRM_NAME,STATUS_NAME,p.STATUS_ID,VALUER_ID');
        $this->db->from('property p');
        $this->db->join('bank b', 'b.id=p.BANK_ID', 'left');
        $this->db->join('users uv', 'uv.id=p.VALUER_ID', 'left');
        $this->db->join('district d', 'd.id=p.DISTRICT_ID', 'left');
        $this->db->join('firms frm', 'frm.id=p.FIRM_ID', 'left');
        $this->db->join('users uc', 'uc.id=p.CREATED_BY', 'left');
        $this->db->join('users um', 'um.id=p.MODIFIED_BY', 'left');
        $this->db->join('status st', 'st.id=p.STATUS_ID', 'left');
        $this->db->order_by('p.id DESC');
        if ($filter === FALSE) {
            $query = $this->db->get();
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('p.id=' . $filter);
                $query = $this->db->get();
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get();
               // print_r($this->db->last_query());die;
                return $query->result_array();
            }
        }
    }

    public function set($serial_no) {
            $data = $this->input->post(NULL, TRUE);
            unset($data['id'],$data['tbl']);
            //$date_of_val = explode('-', $this->input->post('DATE_OF_VAL'), 3);
            //$data['DATE_OF_VAL'] = count($date_of_val) === 3 ? ($date_of_val[2] . "-" . $date_of_val[1] . "-" . $date_of_val[0]) : null;
           if($_POST['BANK_ID']=="9999"){
            $data['BANK_OPTION'] = $this->input->post('BANK_OPTION');
            }

            $data['SERIAL_NO'] = $serial_no;
            $data['CREATED_BY'] =$_SESSION['id'];
            $this->db->insert('property', $data);
            return $this->db->insert_id();
    }

    public function update() {
        $data = $this->input->post(NULL, TRUE);
            unset($data['id'],$data['tbl']);
         if($_POST['BANK_ID']=="9999"){
            $data['BANK_OPTION'] = $this->input->post('BANK_OPTION');
            }
            $data['MODIFIED_BY'] =$_SESSION['id'];
            //$date_of_val = explode('-', $this->input->post('DATE_OF_VAL'), 3);
            //$data['DATE_OF_VAL'] = count($date_of_val) === 3 ? ($date_of_val[2] . "-" . $date_of_val[1] . "-" . $date_of_val[0]) : null;
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update('property', $data);
    }

    public function change_status($id = false) {

        $data = array('STATUS_ID' =>$this->input->post('status_id'));
        $this->db->where('id', $id);
        $query = $this->db->update('property',$data);
        if ($query) {
            return true;
        } else {
            return false;
        }
        
    }

    public function delete_by_id($id = false) {

        $data = array('STATUS_ID' =>'8');
            $this->db->where('id', $id);
            $query = $this->db->update('property',$data);
            if ($query) {
                return true;
            } else {
                return false;
            }
    }
}
