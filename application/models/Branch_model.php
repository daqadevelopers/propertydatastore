<?php
class Branch_model extends CI_Model {

    Public function __construct()
    {
      parent :: __construct();
      $this->table = 'branches';
    }

    public function get($filter = FALSE) {
        $this->db->select('b.id,BRANCH_NAME,BRANCH_ADDRESS,b.status_id,STATUS_NAME');
        $this->db->from('branches b');
        $this->db->join('status st', 'st.id=b.STATUS_ID', 'left');
        if ($filter === FALSE) {
            $query = $this->db->get();
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('branches.id=' . $filter);
                $query = $this->db->get();
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get();
                return $query->result_array();
            }
        }
    }

    public function set() {
        $data = $this->input->post(NULL, TRUE);
        unset($data['id']);
        $data['status_id'] = '1';
        $data['FIRM_ID'] = $_SESSION['FIRM_ID'];
        $data['date_created'] = time();
        $data['created_by'] = $_SESSION['id'];
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $id=$this->input->post('id');
        $data = $this->input->post(NULL, TRUE);
        unset($data['id']);
        $data['modified_by'] = $_SESSION['id'];
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update($this->table, $data);
    }

    public function change_status($id = false) {
        $data = array('status_id' =>$this->input->post('status_id'));
        $this->db->where('id', $id);
        $query = $this->db->update('branches',$data);
        if ($query) {
            return true;
        } else {
            return false;
        }
        
    }

    public function delete_by_id($id = false) {

        $data = array('status_id' =>'8');
            $this->db->where('id', $id);
            $query = $this->db->update('branches',$data);
            if ($query) {
                return true;
            } else {
                return false;
            }
    }

}
