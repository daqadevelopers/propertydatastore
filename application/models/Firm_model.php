<?php

class Firm_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get($filter = FALSE) {
        if ($filter === FALSE) {
            $query = $this->db->get('firms');
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('id=' . $filter);
                $query = $this->db->get('firms');
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get('firms');
                return $query->result_array();
            }
        }
    }

 

}
