<?php 

/**
 * Author : Ajuna Reagan
 */
class Staff_model extends CI_Model {   
	var $subQuery ='';
    private $single_contact;

    public function __construct() {
        parent :: __construct();
        $this->load->database();
        $this->single_contact = "
                (SELECT `USER_ID`, `MOBILE_NUMBER` FROM `contact`
                WHERE `id` in (SELECT MAX(`id`) from `contact` GROUP BY `USER_ID`)
            )";
    }
	
	 /**
     * This method displays staff data from the database
     */
    public function get($filter = false) {
        $this->db->select('u.id,u.SALUTATION,u.FIRSTNAME,u.PHOTO,u.PASS_CHECK as check,u.LASTNAME,u.OTHERNAMES,u.GENDER,u.EMAIL,u.STATUS_ID,u.ROLE_ID,r.ROLE_NAME,u.CREATED_AT,u.INITIALS,u.COMMENT,st.STATUS_NAME,u.BRANCH_ID,cr.FIRSTNAME as createdby_firstname,cr.LASTNAME as createdby_lastname,md.FIRSTNAME as modifiedby_firstname,md.LASTNAME as modifiedby_lastname,u.CREATED_AT,u.DATE_MODIFIED,u.FIRM_ID,frm.FIRM_NAME,c.MOBILE_NUMBER,b.BRANCH_NAME');
        $this->db->from('users u');
        $this->db->join('users cr', 'cr.id=u.CREATED_BY', 'left');
        $this->db->join('users md', 'md.id=u.MODIFIED_BY', 'left');
        $this->db->join('roles r', 'u.ROLE_ID=r.id','left');
        $this->db->join('status st', 'st.id=u.STATUS_ID', 'left');
        $this->db->join('firms frm', 'frm.id = u.FIRM_ID', 'left');
        $this->db->join('branches b', 'b.id = u.BRANCH_ID', 'left');
        $this->db->join("$this->single_contact c", "c.USER_ID = u.id", "left");
        $this->db->where('u.status_id!=9');

        $this->db->order_by('u.id DESC');
        if ($filter === FALSE) {
            $query = $this->db->get();
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('u.id=' . $filter);
                $query = $this->db->get();
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get();
                //print_r($this->db->last_query());die;
                return $query->result_array();
            }
        }
    }

   public function set($passcode) {
        $data = $this->input->post(NULL, TRUE);
        unset($data['id'], $data['tbl']);
        // PASSWORD 
        if(!empty($passcode)){
            $rawpassword = $passcode;
            $options = [
                'cost' => 12,
            ];
            $password =password_hash($rawpassword, PASSWORD_BCRYPT, $options);
            $data['PASSWORD'] =  $password;
        }
        $data['CREATED_BY'] = $_SESSION['id'];
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function update() {
        $id = $this->input->post('id');
        $data = $this->input->post(NULL, TRUE);
        unset($data['id'], $data['tbl']);
        $data['MODIFIED_BY'] = $_SESSION['id'];

        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->update('users', $data);
        } else {
            return false;
        }
    }
     //Get registered by
    public function get_valued_by($filter = FALSE) {
        $this->db->select('id,FIRSTNAME,LASTNAME,OTHERNAMES,SALUTATION,INITIALS');
        $query = $this->db->from('users');
        $this->db->where('users.STATUS_ID IN (1,4)');
        if ($filter === FALSE) {
            $query = $this->db->get();
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('users.id=' . $filter);
                $query = $this->db->get('', 1);
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get();
                return $query->result_array();
            }
        }
    }

    
      /**
     * This method deactivate staff data from the database
     */
    public function change_status_by_id($id = false) {

            $data = array('STATUS_ID' =>$this->input->post('status_id'));
            $this->db->where('id', $id);
            $query = $this->db->update('users',$data);
            if ($query) {
                return true;
            } else {
                return false;
            }
        
    }

    public function delete_by_id($id = false) {

           $data = array('STATUS_ID' =>'8');
            $this->db->where('id', $id);
            $query = $this->db->update('users',$data);
            if ($query) {
                return true;
            } else {
                return false;
            }
    }
}
?>