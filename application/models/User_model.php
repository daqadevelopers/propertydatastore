<?php

/**
 * This class helps to create accounts and logins
 */
class User_model extends CI_Model {

    Public function __construct() {
        parent :: __construct();
    }

    // login the user
    public function login() {
        $this->db->select('u.id,u.SALUTATION,u.FIRSTNAME,u.PHOTO,u.LASTNAME,u.OTHERNAMES,u.PASSWORD,u.GENDER,u.EMAIL,u.STATUS_ID,u.FIRM_ID,frm.FIRM_NAME,u.ROLE_ID,r.ROLE_NAME,c.MOBILE_NUMBER,u.BRANCH_ID,b.BRANCH_NAME');
        $this->db->from('users u');
        $this->db->join('firms frm', 'u.FIRM_ID=frm.id','left');
        $this->db->join('roles r', 'u.ROLE_ID=r.id','left');
        $this->db->join('branches b', 'b.id = u.BRANCH_ID', 'left');
        $this->db->join('contact c', 'c.USER_ID=u.id', 'left');
        $this->db->where('u.EMAIL', $this->input->post('username'));
        $this->db->or_where('c.MOBILE_NUMBER', $this->input->post('username'));
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function get_by_uname_email($uname_email = FALSE) {
        $uname_email1 = $uname_email ? $uname_email : $this->input->post('uname_email');
        $this->db->from('user');
        //$this->db->where("status", 1);
        $this->db->where("username", $uname_email1);
        $this->db->or_where("email", $uname_email1);
        $this->db->or_where("email2", $uname_email1);
        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * This method updates staff data in the database
     */
    public function update_user() {
        $id = $this->input->post('user_id');
        $data = $this->input->post(NULL, TRUE);
        //Date Registered
        if (isset($data['date_registered']) && $data['date_registered'] != '') {
            $registration_date = explode('-', $data['date_registered'], 3);
            $data['date_registered'] = count($registration_date) === 3 ? ($registration_date[2] . "-" . $registration_date[1] . "-" . $registration_date[0]) : null;
        }

        //Date of Birth
        $date_of_birth = explode('-', $data['date_of_birth'], 3);
        $data['date_of_birth'] = count($date_of_birth) === 3 ? ($date_of_birth[2] . "-" . $date_of_birth[1] . "-" . $date_of_birth[0]) : null;

        //Unsetting the values
        unset($data['id'], $data['user_id'],$data['occupation'],$data['confirmpassword'],$data['password'],$data['spouse_name'],$data['subscription_plan_id'], $data['position_id'],$data['mobile_number']);
        $data['modified_by'] = $_SESSION['staff_id'];
        $this->db->where('id', $id);
        $query = $this->db->update('user', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function change_status() {

        $data = array(
            'status_id' => $this->input->post('status') !== NULL ? $this->input->post('status') : 0,
            'modified_by' => $_SESSION['user_id']
        );
        $this->db->where('user.id', $this->input->post('user_id') !== NULL ? $this->input->post('user_id') : $_SESSION['user_id']);
        return $this->db->update('user', $data);
    }

    public function delete($user_id) {
        $this->db->where('id', $user_id);
        return $this->db->delete('user');
    }




      public function check_user($email)
      {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->where('users.EMAIL',$email);
     
          if($query=$this->db->get())
          {
              return $query->row_array();
          }
          else{
            return false;
          }
      
      }
      
      public function fetch_user_id($f_email,$f_code)
      {
       $this->db->select('*');
       $this->db->from('users');
      $this->db->where('F_EMAIL',$f_email);
      $this->db->where('F_CODE',$f_code);

      if($query=$this->db->get())
      {
          return $query->row_array();
      }
      else{
        return false;
      }
      
      }
      
      
      
      public function check_user_approved($email)
      {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->join('pathway','users.PATH_ID=pathway.PATH_ID','LEFT');
       $this->db->join('registration','users.UNIQUE_ID=registration.UNIQUE_ID','LEFT');
       $this->db->join('srb_registration','users.UNIQUE_ID=srb_registration.UNIQUE_ID','LEFT');
       $this->db->where('users.EMAIL',$email);
       $this->db->where('users.USER_STATUS','Active');
     
     

      if($query=$this->db->get())
      {
          return $query->row_array();
      }
      else{
        return false;
      }
      
      }
      
      
      public function check_activate($f_email,$f_code)
      {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->where('F_EMAIL',$f_email);
       $this->db->where('F_CODE',$f_code);

      if($query=$this->db->get())
      {
          return $query->row_array();
      }
      else{
        return false;
      }
      
      }
      
      
    public function update_user_table($Q_email,$data)
    {   
         
          $this->db->where('EMAIL',$Q_email);
          return $this->db->update('users',$data); 
         
    }

    public function get_session_log($filter=FALSE) {
        $this->db->select('s.*,u.id as USER_ID,u.SALUTATION,u.FIRSTNAME,u.PHOTO,u.LASTNAME,u.OTHERNAMES,u.FIRM_ID');
        $this->db->from('session_log s');
        $this->db->join('users u', 's.USER_ID=u.id', 'left');
        $this->db->order_by("s.LASTSEEN","desc");
        if ($filter === FALSE) {
        $this->db->where('u.status_id!=9');
            $query = $this->db->get();
            return $query->result_array();
        } else {
            if (is_numeric($filter)) {
                $this->db->where('s.USER_ID=' . $filter);
                $query = $this->db->get();
                return $query->row_array();
            } else {
                $this->db->where($filter);
                $query = $this->db->get();
                return $query->result_array();
            }
        }
    }
    public function insert_session($data) {
      $ss= $this->db->insert('session_log', $data);
        if($ss){
            return true;
        }else{
            return false;
        }
    }
    public function update_session($id,$data) {
      
        if (is_numeric($id)) {
            $this->db->where('USER_ID', $id);
             if($this->db->update('session_log', $data)){
                 return true; 
             }else{
                return false;
             }
        } else {
            return false;
        }
    }

}
