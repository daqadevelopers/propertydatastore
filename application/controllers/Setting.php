<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->library("helpers");
        if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
        $this->helpers->log_online_status_create($this->session->userdata('id'));
        

    }

    public function index() {
        $this->data['title'] = $this->data['sub_title'] = "Organisation /Firm Setup";
        
        $neededjs = array("plugins/select2/select2.full.min.js", "plugins/validate/jquery.validate.min.js");
        $neededcss = array("fieldset.css","plugins/select2/select2.min.css");

        $this->helpers->dynamic_script_tags($neededjs, $neededcss);

        $this->template->title = $this->data['title'];
        $this->template->content->view('settings/index', $this->data);
        // Publish the template
        $this->template->publish();
    }
}
