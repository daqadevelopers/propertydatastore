<?php

// Author : Ajuna

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Staff_model');
          $this->load->model("contact_model");
          $this->load->library('helpers');
          $this->load->model(array('Property_model','Firm_model'));
              if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
        $this->helpers->log_online_status_create($this->session->userdata('id'));

   }

	public function index()
	{
   
        $neededjs = array();
        $neededcss = array();
        $this->data['properties'] = $this->Property_model->get();
        $xx=array();
         for($i=1;$i<=12;$i++){ //props this year
              $this->data[$i] = $this->Property_model->get("MONTH(`DATE_OF_VAL`) =".$i." AND YEAR(`DATE_OF_VAL`) = ".date("Y"));
                $xx[$i]=count( $this->data[$i]);
         }
        $this->data['month']=$xx;
        $this->data['monthly'] =$this->Property_model->get_per_month(date('Y'));
        $no_cord = $this->Property_model->get('(NORTH IS NULL OR NORTH = "") OR (EAST IS NULL OR EAST ="") ');
        $this->data['properties_nocord']=count($no_cord);
        $this->data['properties_cord'] =count($this->data['properties'])-$this->data['properties_nocord'];

        $this->data['per_dist'] = $this->Property_model->get_distinct();
      
        $this->data['user'] = $this->Staff_model->get();
        $this->data['valuers'] = $this->Staff_model->get_valued_by();
       // print_r($this->data);
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('dashboard', $this->data);
        // Publish the template
		$this->template->publish();
      
	}
}
