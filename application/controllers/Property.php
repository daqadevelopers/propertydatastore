<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Property extends CI_Controller {
	public function __construct() {
		parent::__construct();
        $this->load->library('helpers');
		$this->load->model(array('Property_model','District_model','Bank_model','Staff_model','Firm_model'));
        if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
        $this->helpers->log_online_status_create($this->session->userdata('id'));
	}

     public function jsonList() {
        $this->data['data'] = $this->Property_model->get("p.STATUS_ID=".$this->input->post('status_id'));
        echo json_encode($this->data);
    }
     public function prop_per_user() {
        $this->data['data'] = $this->Property_model->get_per_user("p.STATUS_ID=".$this->input->post('status_id'));
        echo json_encode($this->data);
    }

    public function prop_per_entrant() {
         $this->data['this_week'] = $this->Property_model->get_per_entrant(" WHERE (DATE_CREATED BETWEEN '".date('Y-m-d',strtotime('this week monday'))."' AND '".date('Y-m-d',strtotime('this week sunday'))."')");
       
        echo json_encode($this->data);
    }
	public function index()
	{
		$neededjs = array("plugins/datepicker/bootstrap-datepicker.js","plugins/validate/jquery.validate.min.js","plugins/jquery-ui.js");
        $neededcss = array("plugins/datepicker/datepicker3.css","plugins/jquery-ui.css");
		
		$this->data['properties'] = $this->Property_model->get();
		$this->data['districts'] = $this->District_model->get();
        $this->data['banks'] = $this->Bank_model->get();
        $this->data['firms'] = $this->Firm_model->get();
        $this->data['valuers'] = $this->Staff_model->get_valued_by();
		//print_r($this->data['valuers']); die();
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('property/index', $this->data);
        // Publish the template
		$this->template->publish();
		
	}

	 public function create() {

        $this->form_validation->set_rules('TENURE', 'Tenure', 'required');
        $this->form_validation->set_rules('DATE_OF_VAL', 'Date of Valuation', array('required'), array('required' => '%s must be selected'));
        $this->form_validation->set_rules('ACREAGE', 'Acreage', 'required');
        $this->form_validation->set_rules('RATE_PER_ACRE', 'Rate per acre', 'required');
        $this->form_validation->set_rules('PROPERTY_VALUE', 'Property Value', 'required');
        $this->form_validation->set_rules('DISTRICT_ID', 'District', 'required');
        $this->form_validation->set_rules('USER_STATUS', 'User', 'required');
        if(isset($_POST['ZONE'])==""){
            $this->form_validation->set_rules('ZONE', 'Hemisphere', array('required'), array('required' => 'Please select %s')); 
        }
        if($_POST['NORTH']!=="" && $_POST['EAST']!==""){
              $this->form_validation->set_rules('CONVNORTH', 'Coordinates', array('required'), array('required' => '%s format do not match'));
              $this->form_validation->set_rules('CONVEAST', 'Coordinates', array('required'), array('required' => '%s format do not match'));
        }
        
        if($_POST['NORTH']!==""){
             $this->form_validation->set_rules('EAST', ' East Cordinate', 'required');    
        }
        if($_POST['EAST']!==""){
             $this->form_validation->set_rules('NORTH', ' North Cordinate', 'required');
        }
        if($_POST['USER_STATUS']=="Other"){
             $this->form_validation->set_rules('USER_OPTION', 'Specify Other User', 'required');
        }
        if($_POST['BANK_ID']=="9999"){
             $this->form_validation->set_rules('BANK_OPTION', 'Please Specify Bank name...', 'required');
        }

        $feedback['success'] = false;
        if ($this->form_validation->run() === FALSE) {
            $feedback['message'] = validation_errors();
            
        } else {
            if (isset($_POST['id']) && is_numeric($_POST['id'])) {

                if ($this->Property_model->update()) {
                    $feedback['success'] = true;
                    $feedback['message'] = "Property successfully updated";
                   
                } else {
                    $feedback['message'] = "There was a problem updating the Property";
                }
            } else {

                $serial_no =$this->generate_serial_no();
                if (is_numeric($this->Property_model->set($serial_no))) {
                    $feedback['success'] = true;
                    
                    $feedback['message'] = "Property successfully saved";
                } else {
                    $feedback['message'] = "There was a problem saving the Property";
                }
            }
        }
        echo json_encode($feedback);
    }

    public function view($id) {
        $this->data['property'] = $this->Property_model->get($id);
        if (empty($this->data['property'])) {
            show_404();
            }
            $this->data['properties'] = $this->Property_model->get("CONVNORTH !=''");
            $this->data['districts'] = $this->District_model->get();
            $this->data['banks'] = $this->Bank_model->get();
            $this->data['firms'] = $this->Firm_model->get();
            $this->data['valuers'] = $this->Staff_model->get_valued_by();
            $this->data['title'] ="";
            $this->data['sub_title'] = "";

        $this->template->title = $this->data['title'];
 
        $this->template->content->view('property/details', $this->data);
        // Publish the template
        $this->template->publish();
    }

    function generate_serial_no() {
    $this->data['serial_counter'] =$this->Firm_model->get($_SESSION['FIRM_ID']);
    $org_id = $this->data['serial_counter']['id'];
    $counter =  $this->data['serial_counter']['COUNTER'];   
    if ($counter == 9999) {
            $counter=0;
        $serial_no = $org_id.date('ym'). sprintf("%04d", $counter + 1);
    } else {
        $serial_no = $org_id.date('ym'). sprintf("%04d", $counter + 1);
    }
    $this->db->where('id',$org_id);
    $upd = $this->db->update('firms', ["COUNTER"=> $counter+1]);
    return $serial_no;
    }

    public function delete(){
        $response['message'] = "Property could not be deleted, contact support.";
        $response['success'] = FALSE;
        if($this->Property_model->delete_by_id($this->input->post('id'))){
          $response['success'] = TRUE;
          $response['message'] = "Property successfully deleted.";
        }
        echo json_encode($response);
      }
  
      public function change_status(){
          $response['success'] = FALSE;
          $response['message'] = "Property not deactivated.";
        if($this->Property_model->change_status($this->input->post('id'))){
          $response['success'] = TRUE;
          if($this->input->post('status_id')==1){
            $response['message'] = "Property successfully Activated";
            } else {
            $response['message'] = "Property successfully deactivated";          
            }
        }
        echo json_encode($response);
      }
	 
}
