<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends CI_Controller {
	public function __construct() {
		parent::__construct();
	
        if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
       
	}
 
    public function index()
	{ 
		$neededjs = array("");
        $neededcss =array("");
        
        $data['heading'] = "ERROR!!";
        $data['message']= "Sorry,we cant find what you are looking for";
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('errors/html/custom_404', $data);
        // Publish the template
        $this->template->publish();
		
	}


}
