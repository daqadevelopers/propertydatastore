<?php

// Author : Ajuna

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
	}

	public function index()
	{
        if (!empty($this->session->userdata('id'))) {
            redirect('dashboard');
        } else{
        $this->load->view('users/login');
        }
	}

    public function auth($msg_type = false) {
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($msg_type == 1) {
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center alert-dismissable">Successfully Logged out <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
        } elseif ($msg_type == 2) {
            $this->session->set_flashdata('message', '<div class="alert alert-warning text-center alert-dismissable">Logged out due to inactivity, login again <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
        } elseif ($msg_type == 3) {
            $this->session->set_flashdata('message', '<div class="alert alert-primary text-center alert-dismissable">Please login to access this resource. <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
        } elseif ($msg_type == 4) {
            $this->session->set_flashdata('message', '<div class="alert alert-primary text-center alert-dismissable">You do not have sufficient privilleges.<br/>Please contact the administrator first. <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
        }

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('users/login');
        } else {
            $this->data['users'] = $this->user_model->login();
            //print_r($this->data['users']); die();
            if (empty($this->data['users'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger text-center alert-dismissable">Incorrect Username or Password ! <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
                $this->load->view('users/login',$this->data);
            } else {
                if(($this->data['users']['STATUS_ID']==1)||($this->data['users']['STATUS_ID']==9)){
                if (password_verify($this->input->post('password'), $this->data['users']['PASSWORD'])) {
                    //Login successful
                    $hash = password_hash($this->input->post('password'), PASSWORD_DEFAULT, ['cost' => 12]);
                    if (password_needs_rehash($hash, PASSWORD_DEFAULT, ['cost' => 12])) {
                        // Recalculate a new password_hash() and overwrite the one we stored previously
                        $this->user_model->update_pass($this->data['users']['id']);
                    }
                    // Add user data in session
                    $userdata = array(
                        'id' => $this->data['users']['id'],
                        'SALUTATION' => $this->data['users']['SALUTATION'],
                        'FIRSTNAME' => $this->data['users']['FIRSTNAME'],
                        'LASTNAME' => $this->data['users']['LASTNAME'],
                        'OTHERNAMES' => $this->data['users']['OTHERNAMES'],
                        'GENDER' => $this->data['users']['GENDER'],
                        'EMAIL' => $this->data['users']['EMAIL'],
                        'PHOTO' => $this->data['users']['PHOTO'],
                        'BRANCH_NAME' => $this->data['users']['BRANCH_NAME'],
                        'ROLE_ID' => $this->data['users']['ROLE_ID'],
                        'ROLE_NAME' => $this->data['users']['ROLE_NAME'],
                        'MOBILE_NUMBER' => $this->data['users']['MOBILE_NUMBER'],
                        'curr_interface' => "staff",
                        'FIRM_ID' => $this->data['users']['FIRM_ID'],
                        'FIRM_NAME' => $this->data['users']['FIRM_NAME']
                    );
                    $this->session->set_userdata($userdata);
                    /* if($this->session->userdata('page_url')){
                      redirect($this->session->userdata('page_url'));
                      } else {

                      } */
                      $this->log_online_status_create1($this->data['users']['id']);
                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger text-center">Incorrect Username or Password !</div>');
                    $this->load->view('users/login',$this->data);
                }
                }else{
                 $this->session->set_flashdata('message', '<div class="alert alert-danger text-center alert-dismissable">Account Inactive !<br/>Please contact the administrator .  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button></div>');
                $this->load->view('users/login',$this->data);
                }
               
            }
        }
    }

    function log_online_status_create1($id){
        $this->data['sessions'] = $this->user_model->get_session_log($id);
        //print_r($this->data['sessions']).die;
        if (count($this->data['sessions']) >= 1) {

            //update
            $data = array('TIME'=>time(),
                        'STATUS'=>1,
                        'LASTSEEN'=>time()
                    );
            $this->user_model->update_session($id, $data);
        } else {
            //insert
            $data = array('TIME'=>time(),
                        'USER_ID'=>$this->session->userdata('id'),
                        'STATUS'=>1,
                        "LASTSEEN"=>time()
                        );
            $this->user_model->insert_session($data);
        }
    }
    public function logout($msg_type = 1)
    {

        $dataArray = array(
            "LASTSEEN" => time(),
            'STATUS' => '0'
        );
        $this->user_model->update_session($this->session->userdata('id'), $dataArray);
        $this->session->sess_destroy();
        redirect("login/auth/$msg_type", "refresh");
    }
    
    public function checkmail(){
         $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('email', 'Email', 'trim|required', array('required' => ' %s address is required.'));
         $feedback['success'] = false;
        if($this->form_validation->run() == FALSE)
        {
            $feedback['message'] = validation_errors('<li>', '</li>');
         }else{

         $data = array(
            'EMAIL' => $this->input->post('email')
            );
            $res = $this->user_model->check_user($data['EMAIL']);

            if(!empty($res))
            {
            if($this->sendpasscode($res['EMAIL'],$this->generateRandomString())){
              $feedback['success'] = true;
              $feedback['message'] = "We have sent you a link to reset your password, please check your Email";
            } else{
               $feedback['message'] = "There was a problem sending the email, please try again";
            }
            } else {
               $feedback['message'] = "Your email does not match any account, Try the email you used to create this Account";
            } 
           
       }
       echo json_encode($feedback);
    }

    
    public function set_password(){
     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]', array('required' => 'Please set your %s.','min_length[6]'=>'%s must be alteast 6 Characters'));
       $this->form_validation->set_rules('repass', 'Confirm Password', 'trim|required|matches[pass]', array('required' => 'Please confirm your password.',' matches[pass]'=>'%s does not match'));
        $f_email=$_GET['f_e'];
        $f_code=$_GET['r_c'];
        $user =$this->user_model->fetch_user_id($f_email,$f_code);
         if(!empty($user)){
             
        if($this->form_validation->run() == FALSE)
         {
        $this->load->view('users/reset_pass');
         }else{
           
         $id=$user['id'];
            $rawpassword = $this->input->post('repass');
            $options = [
                'cost' => 12,
            ];
            $password =password_hash($rawpassword, PASSWORD_BCRYPT, $options);
           $data = array(
            'F_EMAIL' => NULL,
            'F_CODE' => NULL,
            'PASSWORD' => $password
            );
            $this->db->where('id',$id);
            if($this->db->update('users',$data))
            {
                 $this->session->set_flashdata('message','<div class="alert alert-success text-center">Successful ! Login with the new password now </div>');
              $this->load->view('users/login');
                
            } else{
                 $this->session->set_flashdata('rpass2','<div class="alert alert-warning text-center">Failed, Try again</div>');
              $this->load->view('users/reset_pass');
               
            } 
           
       }
         
    } else {
         $this->session->set_flashdata('rpass2','<div class="alert alert-danger text-center">The Link has already been used by you </div>');
               $this->load->view('users/reset_pass');
    }
    }
    
    function sendpasscode($Q_email,$emailcode){


       $code_to_email=md5($emailcode);
       $encode_mail =md5($Q_email); 
         
         $data = array(
        'F_CODE' => $code_to_email,
        'F_EMAIL' => $encode_mail,
         );
        
    $subject='FORGOT PASSWORD REQUEST';
    $send_to_list = array($this->input->post('email'));
    $message ='<tr bgcolor="#890606">
            <td >
                <h1><center>RESET PASSWORD</center></h1>
            </td>
        </tr>
        <tr>
            <td class="content-block">
                <h3>Hello '.$Q_email.',</h3>
            </td>
        </tr>
     <tr>
        <td class="content-block">
        You have requested a password reset, please follow the link below to reset your password.
            </td>
        </tr>
        <tr>
            <td class="content-block">
               Please ignore this email if you did not request a password change.
            </td>
        </tr>
       
        <tr>
            <td class="content-block aligncenter">
                <a target="_blank" href="https://www.dotcomvalues.org/login/set_password?r_c='.$code_to_email.'&f_e='.$encode_mail.'" class="btn-info">Reset password</a>
            </td>
        </tr>
        <tr>
            <td class="content-block ">
                 OR copy this link and paste in your browser https://www.dotcomvalues.org/login/set_password?r_c='.$code_to_email.'&f_e='.$encode_mail.'
            <p>With Regards</p>
            <p>EACSV SYSTEM ADMIN</p>
            </td>
        </tr>';
       if($this->helpers->send_email($send_to_list,$subject,$message)){
        if($this->user_model->update_user_table($Q_email,$data)){
            return true;
        } 
        } else{
          return false;
        }
    }


    function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function jsonList() {
        $datas = $this->user_model->get_session_log();
        echo json_encode($datas);
    }
    
 
}
