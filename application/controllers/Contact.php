<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
  
    public function __construct() {
     parent::__construct(); 
     $this->load->library("session");
     if(empty($this->session->userdata('id'))){
        redirect('welcome/login/3');
    } 
        $this->load->model("contact_model");
        $this->load->model("user_model");
    }
    public function jsonList(){
        $this->data['data'] = $this->contact_model->get( $this->input->post('user_id') );
        echo json_encode($this->data);
    }

#user_id, mobile_number, contact_type_id, date_created, date_modified, created_by, modified_by
    public function create(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules("MOBILE_NUMBER", "Phone Number", "required|callback__check_phone_number", array("required" => "%s must be entered","_check_phone_number" => "%s already exists"));
      $this->form_validation->set_rules('CONTACT_TYPE_ID', 'Contact type', 'required');
      $feedback['success'] = false;
      
        if($this->form_validation->run() === FALSE ){
        $feedback['message'] = validation_errors('<li>','</li>');
            }else{
                if($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))){ //editing contact
                    
                    if($this->contact_model->update_contact()){
                          $feedback['success'] = true;
                          $feedback['message'] = "Contact Details successfully updated";
                        }else{
                          $feedback['message'] = "There was a problem updating the contact data, please try again";
                        }
                }else{
                    //adding a new item
                        if($this->contact_model->add_contact()){
                          $feedback['success'] = true;
                          $feedback['message'] = "Contact has been successfully Added";
                        }else{
                          $feedback['message'] = "There was a problem saving the contact data, please try again";
                        }
                }
            }
        echo json_encode($feedback);
    }

    function _check_phone_number($phone_number) {
        $existing_number = $this->contact_model->validate_contact($phone_number);
        
        return $existing_number;
    }
    public function delete(){
      $response['message'] = "Data could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->contact_model->delete_by_id()){
        $response['success'] = TRUE;
        $response['message'] = "Data successfully deleted.";
      }
      echo json_encode($response);
    }

  
}
