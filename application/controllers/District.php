<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_Controller {
	
    public function __construct() {
		 parent::__construct(); 
        $this->load->model("District_model");
    }
    public function jsonList(){
        $this->data['data'] = $this->District_model->get("d.status_id IN(0,1,4)");
        echo json_encode($this->data);
    }

     public function create(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('DISTRICT_NAME', 'District Name', 'required');
      $feedback['success'] = false;
      
        if($this->form_validation->run() === FALSE ){
        $feedback['message'] = validation_errors('<li>','</li>');
        
            }else{
                if($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))){ //editing exsting item

                  if($this->District_model->update()){
                    $feedback['success'] = true;
                    $feedback['message'] = "District details successfully updated";
                  }else{
                    $feedback['message'] = "District details could not be updated";
                  }
                }else{
                  //adding a new user
                  $return_id = $this->District_model->set();
                  if(is_numeric($return_id)){
                    $feedback['success'] = true;
                    $feedback['message'] = "District details submitted";

                  }else{
                    $feedback['message'] = "There was a problem saving the District details, please contact IT support";

                  }
                }
            }
        echo json_encode($feedback);
    }
    public function delete(){
      $response['message'] = "District could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->District_model->delete_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "District successfully deleted.";
      }
      echo json_encode($response);
    }

    public function change_status(){
        $response['success'] = FALSE;
        $response['message'] = "District not deactivated.";
      if($this->District_model->change_status($this->input->post('id'))){
        $response['success'] = TRUE;
        if($this->input->post('status_id')==1){
        $response['message'] = "District successfully Activated";
        } else {
        $response['message'] = "District successfully deactivated";          
        }
      }
      echo json_encode($response);
    }
}
