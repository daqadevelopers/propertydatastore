<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {
	
    public function __construct() {
		 parent::__construct(); 
        $this->load->model("Branch_model");
    }
    public function jsonList(){
        $this->data['data'] = $this->Branch_model->get("b.status_id IN(1,4)");
        echo json_encode($this->data);
    }

     public function create(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('BRANCH_NAME', 'Branch Name', 'required');
      $this->form_validation->set_rules('BRANCH_ADDRESS', 'Branch Address', 'required');
      $feedback['success'] = false;
      
        if($this->form_validation->run() === FALSE ){
        $feedback['message'] = validation_errors('<li>','</li>');
        
            }else{
                if($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))){ //editing exsting item

                  if($this->Branch_model->update()){
                    $feedback['success'] = true;
                    $feedback['message'] = "Branch details successfully updated";
                  }else{
                    $feedback['message'] = "Branch details could not be updated";
                  }
                }else{
                  //adding a new user
                  $return_id = $this->Branch_model->set();
                  if(is_numeric($return_id)){
                    $feedback['success'] = true;
                    $feedback['message'] = "Branch details submitted";

                  }else{
                    $feedback['message'] = "There was a problem saving the Branch details, please contact IT support";

                  }
                }
            }
        echo json_encode($feedback);
    }
    public function delete(){
      $response['message'] = "Branch could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->Branch_model->delete_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "Branch successfully deleted.";
      }
      echo json_encode($response);
    }

    public function change_status(){
        $response['success'] = FALSE;
        $response['message'] = "Branch not deactivated.";
      if($this->Branch_model->change_status($this->input->post('id'))){
        $response['success'] = TRUE;
        if($this->input->post('status_id')==1){
        $response['message'] = "Branch successfully Activated";
        } else {
        $response['message'] = "Branch successfully deactivated";          
        }
      }
      echo json_encode($response);
    }
}
