<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	
    public function __construct() {
		 parent::__construct(); 
        $this->load->model("Bank_model");
    }
    public function jsonList(){
        $this->data['data'] = $this->Bank_model->get("b.status_id IN(0,1,4)");
        echo json_encode($this->data);
    }

     public function create(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('BANK_NAME', 'Bank Name', 'required');
      $feedback['success'] = false;
      
        if($this->form_validation->run() === FALSE ){
        $feedback['message'] = validation_errors('<li>','</li>');
        
            }else{
                if($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))){ //editing exsting item

                  if($this->Bank_model->update()){
                    $feedback['success'] = true;
                    $feedback['message'] = "Bank details successfully updated";
                  }else{
                    $feedback['message'] = "Bank details could not be updated";
                  }
                }else{
                  //adding a new user
                  $return_id = $this->Bank_model->set();
                  if(is_numeric($return_id)){
                    $feedback['success'] = true;
                    $feedback['message'] = "Bank details submitted";

                  }else{
                    $feedback['message'] = "There was a problem saving the Bank details, please contact IT support";

                  }
                }
            }
        echo json_encode($feedback);
    }
    public function delete(){
      $response['message'] = "Bank could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->Bank_model->delete_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "Bank successfully deleted.";
      }
      echo json_encode($response);
    }

    public function change_status(){
        $response['success'] = FALSE;
        $response['message'] = "Bank not deactivated.";
      if($this->Bank_model->change_status($this->input->post('id'))){
        $response['success'] = TRUE;
        if($this->input->post('status_id')==1){
        $response['message'] = "Bank successfully Activated";
        } else {
        $response['message'] = "Bank successfully deactivated";          
        }
      }
      echo json_encode($response);
    }
}
