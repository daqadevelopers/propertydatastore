<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {
	
    public function __construct() {
		 parent::__construct(); 
        $this->load->model("Role_model");
    }
    public function jsonList(){
        $this->data['data'] = $this->Role_model->get("r.status_id IN(1,4)");
        echo json_encode($this->data);
    }

     public function create(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('ROLE_NAME', 'Role Name', 'required');
      $this->form_validation->set_rules('DESCRIPTION', 'Description', 'required');
      $feedback['success'] = false;
      
        if($this->form_validation->run() === FALSE ){
        $feedback['message'] = validation_errors('<li>','</li>');
        
            }else{
                if($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))){ //editing exsting item

                  if($this->Role_model->update()){
                    $feedback['success'] = true;
                    $feedback['message'] = "Role details successfully updated";
                  }else{
                    $feedback['message'] = "Role details could not be updated";
                  }
                }else{
                  //adding a new user
                  $return_id = $this->Role_model->set();
                  if(is_numeric($return_id)){
                    $feedback['success'] = true;
                    $feedback['message'] = "Role details submitted";

                  }else{
                    $feedback['message'] = "There was a problem saving the Role details, please contact IT support";

                  }
                }
            }
        echo json_encode($feedback);
    }
    public function delete(){
      $response['message'] = "Role could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->Role_model->delete_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "Role successfully deleted.";
      }
      echo json_encode($response);
    }

    public function change_status(){
        $response['success'] = FALSE;
        $response['message'] = "Role not deactivated.";
      if($this->Role_model->change_status($this->input->post('id'))){
        $response['success'] = TRUE;
        if($this->input->post('status_id')==1){
        $response['message'] = "Role successfully Activated";
        } else {
        $response['message'] = "Role successfully deactivated";          
        }
      }
      echo json_encode($response);
    }
}
