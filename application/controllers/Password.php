<?php
class Password extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Password_model');
    }
    public function create() {
        $this->form_validation->set_rules('password', 'New Password', 'required|min_length[8]', array('required' => 'Please set  %s.','min_length[8]'=>'%s must be alteast 8 Characters'));
       $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required|matches[password]', array('required' => 'Please confirm password.',' matches[pass]'=>'%s does not match'));
       
        $feedback['success'] = false;
        if ($this->form_validation->run() === FALSE) {
            $feedback['message'] = validation_errors();
        } else {
           
                $pass_id = $this->Password_model->update();
                if ($pass_id) {
                    $feedback['success'] = true;
                    $feedback['message'] = "Password details successfully saved";
                } else {
                    $feedback['message'] = "There was a problem setting user Password";
                }
        }
        echo json_encode($feedback);
    }
}
