<?php

// Author : Ajuna

defined('BASEPATH') OR exit('No direct script access allowed');

class Prop_pa_entrant extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Staff_model');
          $this->load->model("contact_model");
          $this->load->library('helpers');
          $this->load->model(array('Property_model','Firm_model'));
              if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
        $this->helpers->log_online_status_create($this->session->userdata('id'));

   }
   public function jsonList() {
     if(is_numeric($this->input->post('start_date'))&& empty($this->input->post('end_date'))) {
        $this->data['data'] = $this->Property_model->get("p.STATUS_ID=".$this->input->post('status_id')." AND p.CREATED_BY=".$this->input->post('created_by')." AND YEAR(p.DATE_CREATED)=".$this->input->post('start_date'));

        }elseif (is_numeric($this->input->post('start_date'))&&is_numeric($this->input->post('end_date'))) {
        $this->data['data'] = $this->Property_model->get("p.STATUS_ID=".$this->input->post('status_id')." AND p.CREATED_BY=".$this->input->post('created_by')." AND MONTH(p.DATE_CREATED)=".$this->input->post('start_date'));
        }else {
        $this->data['data'] = $this->Property_model->get("p.STATUS_ID=".$this->input->post('status_id')." AND p.CREATED_BY=".$this->input->post('created_by')." AND (p.DATE_CREATED BETWEEN '".$this->input->post('start_date')."' AND '".$this->input->post('end_date')."')");
        }
        echo json_encode($this->data);
    }

	public function index()
	{
   
        $neededjs = array();
        $neededcss = array();
        $this->data['this_week'] = $this->Property_model->get_per_entrant(" WHERE (DATE_CREATED BETWEEN '".date('Y-m-d',strtotime('this week monday'))."' AND '".date('Y-m-d',strtotime('this week sunday'))."')");
        $this->data['last_week'] = $this->Property_model->get_per_entrant(" WHERE (DATE_CREATED BETWEEN '".date('Y-m-d',strtotime('last week monday'))."' AND '".date('Y-m-d',strtotime('last week sunday'))."')");
        $this->data['today'] = $this->Property_model->get_per_entrant(" WHERE DATE_CREATED=".date('Y-m-d'));
        $this->data['yesterday'] = $this->Property_model->get_per_entrant(" WHERE DATE_CREATED=".date('Y-m-d',strtotime('yesterday')));
        $this->data['month'] = $this->Property_model->get_per_entrant(" WHERE MONTH(DATE_CREATED)=".date('m'));
        $this->data['last_month'] = $this->Property_model->get_per_entrant(" WHERE MONTH(DATE_CREATED)=".date('m',strtotime('last month')));
        $this->data['year'] = $this->Property_model->get_per_entrant(" WHERE YEAR(DATE_CREATED)=".date('Y'));
        $this->data['last_year'] = $this->Property_model->get_per_entrant(" WHERE YEAR(DATE_CREATED)=".date('Y',strtotime('last year')));

        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('users/entrant/entrant_dash', $this->data);
        // Publish the template
		$this->template->publish();
      
	}
    public function view($id,$start_date,$end_date=false)
    {   if(empty($id) || $id==""){
            redirect('Error_404');
        } 
        $neededjs = array("plugins/datepicker/bootstrap-datepicker.js","plugins/validate/jquery.validate.min.js","plugins/jquery-ui.js","proj4conversion.js");
        $neededcss = array("plugins/datepicker/datepicker3.css","plugins/jquery-ui.css");
        $this->data['staff_name'] = $this->Staff_model->get($id);
        $this->data['staffid'] = $id;
        if(is_numeric($start_date)&&$end_date==false){
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = null;
        }elseif (is_numeric($start_date)&&is_numeric($end_date)) {
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        }else {
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        }
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('users/entrant/view_details', $this->data);
        // Publish the template
        $this->template->publish();
        
    }
}
