<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map_view extends CI_Controller {
	
    public function __construct() {
		 parent::__construct(); 
        $this->load->model(array('Property_model','District_model','Bank_model','Staff_model','Firm_model'));
    }
    public function index()
	{
		$neededjs = array("plugins/datepicker/bootstrap-datepicker.js","plugins/validate/jquery.validate.min.js","plugins/jquery-ui.js");
        $neededcss = array("plugins/datepicker/datepicker3.css","plugins/jquery-ui.css");
		if(!empty($this->input->post('ZONE'))){
            $ZONE =$this->input->post('ZONE');
        }else {
            $ZONE=NULL;
        }
        $this->data['properties'] = $this->Property_model->get("CONVNORTH !=''");
		$this->data['cordinates'] = array('NORTH' =>$this->input->post('NORTH'),"EAST"=>$this->input->post('EAST'),"CONVN"=>$this->input->post('CONVN'),"CONVE"=>$this->input->post('CONVE'),"ZONE"=>$ZONE);
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('property/map_view', $this->data);
        // Publish the template
		$this->template->publish();
		
	}
 }