<?php 
$this->onlineusers->set_data(array('key'=>$value));
//You can record the username of a logged user (for example) to show it in another page.

//To optimize speed, the data will be set only once, but if you add a new data, it will work. This example will not work:

$userdata['username'] = 'Peter';
$userdata['id'] = 5;
$this->onlineusers->set_data($userdata);

$usrdata['username'] = 'John';
$userdata['id'] = 3;
$this->onlineusers->set_data('John'); //Doesn't work. The user's username is still Peter and id=5

//The example before will not work, but you can force the change setting the second parameter to true:

$userdata['username'] = 'Peter';
$userdata['id'] = 5;
$this->onlineusers->set_data($userdata);

$usrdata['username'] = 'John';
$userdata['id'] = 3;
$this->onlineusers->set_data($usrdata);  //Works. The user's username is now John and id=3

/* Note that you should prevent using it. Every user_id changes requires a new writing to the database file. I suggest you to use the second parameter only after an user authentication.

<strong>This is not a session class. Do not record so much data.</strong>

This optimization was implemented because the user's session can be alive for longer than the TIMEOUT. For example, you have defined a session time to 1 hour and the timeout of this class to 15min, if the user returns after 30 minutes, he/she will still be logged, but not shown in the members online list. So, to prevent this, don't be afraid to use the set_id() function on every controller you have.

Ex:
 */

class Blog extends Controller {
    
    function Blog()    {

        parent::Controller(); 
        $this->load->library('session');
        $this->load->library('UsersOnline');
        $userdata['username'] = $this->session->username;
        $userdata['id']=$this->session->user_id;
        $this->onlineusers->set_data($userdata);

    }

    function index(){}
}