<?php

// Author : Ajuna

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
   public function __construct() {
        parent::__construct();
        $this->load->library('helpers');
        $this->load->model('Staff_model');
        $this->load->model('Branch_model');
         $this->load->model("contact_model");
          $this->load->model("contact_model");
          $this->load->model('Role_model');
        if(empty($this->session->userdata('id'))){
            redirect('login/auth/3');
        } 
        $this->helpers->log_online_status_create($this->session->userdata('id'));
        
   }

   public function index() {
        $this->data['branches'] = $this->Branch_model->get();
        $this->data['roles'] = $this->Role_model->get('STATUS_ID=1');
        $neededjs = array("plugins/validate/jquery.validate.min.js");
        $neededcss = array();
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('users/staff/index',$this->data);
        // Publish the template
        $this->template->publish();
      
    }

    public function jsonList() {
        $this->data['data'] = $this->Staff_model->get("u.STATUS_ID=".$this->input->post('status_id'));
        echo json_encode($this->data);
    }
   
    public function create() {
         $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('LASTNAME', 'Last Name', 'required');        
          $this->form_validation->set_rules('SALUTATION', 'Salutation', 'trim|required', array('required' => ' %s is required.'));
           $this->form_validation->set_rules('GENDER', 'Gender', 'trim|required', array('required' => 'Select your %s.'));
           $this->form_validation->set_rules('BRANCH_ID', 'Branch', 'trim|required', array('required' => '%s is required !.'));
           if ($this->input->post('id') == NULL) {
           $this->form_validation->set_rules('EMAIL', 'Email', 'trim|valid_email|required|is_unique[users.EMAIL]', array('required'=> '%s is required', 'valid_email'=> '%s is invalid', 'is_unique'=>'This %s address already belongs to another account, please contact your administrator for assistance. Do not create the same user with an onother email address'));
            }
        $feedback['success'] = false;

        if ($this->form_validation->run() === FALSE) {
            $feedback['message'] = validation_errors('<li>', '</li>');
        } else {
            if ($this->input->post('id') !== NULL && is_numeric($this->input->post('id'))) { 
                //editing exsting item
            if ($this->Staff_model->update()) {
                $feedback['success'] = true;
                $feedback['message'] = "User Details successfully updated";
                $feedback['user'] = $this->Staff_model->get($this->input->post('id'));
            } else {
                $feedback['message'] = "There was a problem updating the user data, please try again";
            }
                
            } else {
              $passcode = $this->generateRandomString();
               if ($this->Staff_model->set($passcode)) {
                // SEND EMAIL
                $username =$this->input->post("EMAIL");
                $hello=$this->input->post('FIRSTNAME');
                $subject='LOGIN DETAILS';
                $send_to_list = array($this->input->post('EMAIL'));
                $message ='<tr bgcolor="#17a2b8">
                  <td >
                      <h1><center> DOTCOM VALUES </center></h1>
                  </td>
                  </tr>
                  <tr>
                      <td class="content-block">
                          <h3>Hello '.$hello.',</h3>
                      </td>
                  </tr><tr>
                    <td class="content-block">
                    Please use the following details to login and activate your account <br> <b> USERNAME : </b> '.$username.' <br><b>PASSWORD : </b> '.$passcode.'
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            This is a system generated password, please login and go to <b>My Profile</b> to change your password. <br>
                            <font color="brown"><b>NOTE: </b>Copying and pasting the password not recommended.</font>
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            Click the button below to access the login page
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block aligncenter">
                            <a target="_blank" href="https://www.dotcomvalues.org/" class="btn-info">Go to Login Page</a>
                        </td>
                    </tr>';

                $feedback['success'] = true;        
                if($this->helpers->send_email($send_to_list,$subject,$message)){
                $feedback['message'] = "User Details successfully Added";
                } else{
                 $feedback['message'] = "User registered but email not sent, check email address or go to user details and set temporary password";
                }
                } else {
                $feedback['message'] = "There was a problem adding the user data, please try again";
                }

             } 
          }
        
        echo json_encode($feedback);
    }
     public function view($id) {
        $this->data['branches'] = $this->Branch_model->get();
        $this->data['user'] = $this->Staff_model->get($id);
        $this->data['contact_types'] = $this->contact_model->get_contact_type();
        $this->data['roles'] = $this->Role_model->get('STATUS_ID=1');
      $neededjs = array("plugins/validate/jquery.validate.min.js","plugins/cropping/croppie.js");
        $neededcss = array("plugins/cropping/croppie.css");
        $this->helpers->dynamic_script_tags($neededjs, $neededcss);
        // Load a view in the content partial
        $this->template->content->view('users/staff/view',$this->data);
        // Publish the template
        $this->template->publish();
      
    }
   public function delete(){
      $response['message'] = "User could not be deleted, contact support.";
      $response['success'] = FALSE;
      if($this->Staff_model->delete_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "User successfully deleted.";
      }
      echo json_encode($response);
    }

    public function change_status(){
        $response['success'] = FALSE;
        $response['message'] = "User not deactivated.";
      if($this->Staff_model->change_status_by_id($this->input->post('id'))){
        $response['success'] = TRUE;
        $response['message'] = "User successfully deactivated, can not access the system";
      }
      echo json_encode($response);
    }

    public function add_profile_pic() {
      $this->load->helper('file');
      $userid = $this->input->post('i_d');
      $user_name = $this->input->post('user_name');
      $org_id = $this->input->post('firm'); //firm_id
      $imagery = $this->input->post('image');
      if (!empty($imagery) && $userid != "") {
          $data2 = $imagery;
          list($type, $data2) = explode(';', $data2);
          list(, $data2) = explode(',', $data2);
          $data2 = base64_decode($data2);
          $mypath = 'uploads/firm_' . $org_id . '/profile_pics';
          if (!is_dir($mypath)) {
              mkdir('./' . $mypath, 0755, true);
          }
          $imageName = $user_name . time() . rand(10, 256) . '.jpg';
          $db_img_link = $imageName;
          $path_folder = './' . $mypath . '/' . $imageName;
          if (file_put_contents($path_folder, $data2)) {
              $photo = array(
                  'PHOTO' => $db_img_link
              );
            /* if ($path_old_folder != "") {
                  if (file_exists($path_old_folder)) {
                      unlink($path_old_folder);
                  }
              }  */
              $this->db;
              $this->db->where('id', $userid);
              $updte=$this->db->update('users', $photo);
              if ($updte===true) {
                   if($_SESSION['id']===$userid){
                       echo  $this->session->set_userdata('PHOTO', $imageName );
                   } 
                     $feedback['message'] = "Photo Updated";
              } else {
                  $feedback['message'] = "Database Error";
              }
          }
      } else {
          $feedback['message'] = "Failed, Invalid Photo";
      }
      echo json_encode($feedback);
  }
    
    function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
