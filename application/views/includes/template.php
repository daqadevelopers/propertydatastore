<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Property Datastore ">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Property Datastore">
    <meta property="og:title" content="Property Datastore ">
    <meta property="og:url" content="https://www.propertydatastore.com">
    <meta property="og:image" content="">
    <title>DotCom Values -<?php echo $this->template->title->default("PDS"); ?></title>
    <meta property="og:description" content="<?php echo $this->template->description; ?> ">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->template->meta; ?>
        <!-- Toastr style -->
    <link href="<?php echo base_url("assets/css/plugins/toastr/toastr.min.css"); ?>" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo base_url("assets/css/plugins/sweetalert/sweetalert.css"); ?>" rel="stylesheet">
    <!-- Datatables style -->
    <link href="<?php echo base_url("assets/css/plugins/dataTables/datatables.min.css"); ?>" rel="stylesheet">
    <!-- Datepicker style -->
    <link href="<?php echo base_url("assets/css/plugins/datepicker/datepicker3.css"); ?>" rel="stylesheet">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css'); ?> ">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugins/select2/select2.min.css">
    <!-- LOAD JQUERY FROM HERE -->
    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js'); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/select2/select2.full.js"></script>

  <!-- Font-icon css-->
  <link rel="stylesheet" type="text/css" href="<?php echo site_url("assets/font-awesome/css/font-awesome.css"); ?>">
    <script src="<?php echo base_url('assets/js/proj4.js'); ?>"></script>

<!-- <?php //if($this->router->fetch_class()=="property"){
 //loading proj4converter dynamically
  ?> -->
<script type="text/javascript" language="javascript">  
  $().ready(function () {
    var versionUpdate = (new Date()).getTime();  
    var script = document.createElement("script");  
    script.type = "text/javascript";  
    script.src = "<?php echo base_url('assets/js/proj4conversion.js'); ?>?v=" + versionUpdate;  
    document.body.appendChild(script);  
  });
</script> 
<!-- <?php// } ?> -->
  <?php echo $this->template->stylesheet; ?>
</head>

<body class="app sidebar-mini rtl">
  <!--  Loads contents and navbars -->
  <?php
  $this->view('includes/admin_nav');
  $this->view('locate_modal');

  ?>

  <!-- Essential javascripts for application to work-->
  <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
  <!-- The javascript plugin to display page loading on top-->
  <script src="<?php echo base_url('assets/js/plugins/pace.min.js'); ?>"></script>
  <!-- Data tables -->
  <script src="<?php echo base_url("assets/js/plugins/dataTables/datatables.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/js/plugins/dataTables/dataTables.fixedColumns.min.js"); ?>"></script>
  <!-- Sweet alert -->
  <script src="<?php echo base_url("assets/js/plugins/sweetalert/sweetalert.min.js"); ?>"></script>
  <!-- Toastr script -->
  <script src="<?php echo base_url("assets/js/plugins/toastr/toastr.min.js"); ?>"></script>
  <!-- Bootstrap validator script -->
  <script src="<?php echo base_url("assets/js/plugins/validate/validator.min.js"); ?>"></script>
  <!-- Moment JScript -->
  <script src="<?php echo base_url("assets/js/plugins/moment/moment.min.js"); ?>"></script>
  <!-- Moment JScript -->
  <script src="<?php echo base_url("assets/js/plugins/datepicker/bootstrap-datepicker.js"); ?>"></script>
  <!-- Knockout Jscript -->
  <script src="<?php echo base_url("assets/js/plugins/knockout/knockout-3.4.2.js"); ?>"></script>

  <!-- Page specific javascripts-->
  <?php echo $this->template->javascript; ?>
  <script>
    $(document).ready(function() {
      $('.select2').select2({
        dropdownParent: $("#add_property-modal")
      });
    });
  </script>
  <!-- <script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTURido_kO6MfC6GG26PhI-_JufYwxcjw&callback=initMap">
</script> -->
  <!-- Google analytics script-->
  <?php $this->view('includes/helpers'); ?>
</body>

</html>