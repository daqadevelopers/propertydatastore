<script>
  var dTable = {};

  $(document).ready(function() {
    //  $('form#formGroup').validator().on('submit', saveData);
    /* PICK DATA FOR DATA TABLE  */
    var counter = 1;
    dTable['tblPropPerUser'] = $('#tblPropPerUser').DataTable({
      dom: '<"html5buttons"B>lTfgitp',
      "deferRender": true,
      "order": [
        [0, 'DESC']
      ],
      "ajax": {
        "url": "<?php echo site_url("property/prop_per_user"); ?>",
        "dataType": "JSON",
        "type": "POST",
        "data": function(d) {
          d.status_id = 1;
        }
      },
      "columnDefs": [{
        "targets": [3],
        "orderable": false,
        "searchable": false
      }],
      columns: [
        {
          data: 'INITIALS'
        },
        {
          data: 'SALUTATION',
          render: function(data, type, full, meta) {
            return  full.FIRSTNAME + " " + full.LASTNAME + "  " + full.OTHERNAMES+" ("+full.INITIALS+")" ;
          }
        },
        {
          data: 'count'
        },
        {
          data: 'VALUER_ID',
          render: function(data, type, full, meta) {
            var ret_txt = "<a class='badge badge-default' style='margin-right:10px;' href='<?php echo site_url("Property_pa_valuer/index"); ?>/"+full.VALUER_ID+"'>View <i class='fa fa-ellipsis-h'></i></a>";
            return ret_txt;
          }
        }

      ],
      buttons: <?php
                echo "[],";
                ?>

    });
    dTable['tblPropPerUser'].on('order.dt search.dt', function() {
      dTable['tblPropPerUser'].column(0, {
        search: 'applied',
        order: 'applied'
      }).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw( );


  });
</script>