
if ($("#tblProperty_pa_entrant").length && tabClicked === "tab-active") {
  if(typeof(dTable['tblProperty_pa_entrant'])!=='undefined'){
        $("#tab-inactive").removeClass("active");
        $("#tab-active").addClass("active");
        dTable['tblProperty_pa_entrant'].ajax.reload(null,true);
        counter=1;
   }else{
    dTable['tblProperty_pa_entrant'] = $('#tblProperty_pa_entrant').DataTable( {
    "pageLength": 10,
    "searching": true,
    "paging": true,
    "processing": false,
    "order": [[ 0, "desc" ]],
    "responsive": true,
    "dom":"<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +"<'row'<'col-sm-12'tr>>" +"<'row'<'col-sm-5'i><'col-sm-7'p>>",
      buttons: <?php if(($_SESSION['ROLE_ID']==1) ||($_SESSION['ROLE_ID']==5)||($_SESSION['ROLE_ID']==3)) { ?> getBtnConfig('Property Data'), <?php } else { echo "[],"; } ?>
      "ajax": {
        url:"<?php echo site_url("Prop_pa_entrant/jsonList");?>",
        dataType: 'JSON',
        type: 'POST',
        data: function(d){
          d.status_id=1;
          d.created_by='<?php echo $staffid;?>';
          d.start_date='<?php echo $start_date;?>';
          d.end_date='<?php echo $end_date;?>';
        }
      },
        scrollCollapse: true,
      columnDefs: [{
                  "targets": [16],
                  "width": '20%',
                  "orderable": false,
                  "searchable": false
              }],

      columns:[
      {data: 'SERIAL_NO', render: function (data, type, full, meta) {
            return "<a href='<?php echo site_url("property/view"); ?>/" + full.id + "'>" + data + "</a>";
        }
      },
      { data: 'TENURE' },
      { data: 'PROPERTY_ADDRESS', render:function ( data, type, full, meta ) { 
         return "<a href='<?php echo site_url("property/view"); ?>/" + full.id + "'>" + data + "  ( "+ full.DISTRICT_NAME +" ) </a>";}},
      { data: 'DISTRICT_NAME' },
      { data: 'TOWN_ID'},
      { data: 'VILLAGE_ID'},
      {data: 'BANK_NAME', render: function (data, type, full, meta) {
            if(parseInt(full.BANK_ID)===9999){
            return full.BANK_OPTION;
            } else {
            return data;
            }
        }
      },
      { data: 'NORTH'},
      { data: 'EAST' },
      { data: 'ACREAGE' },
      {data: 'RATE_PER_ACRE', render: function (data, type, full, meta) {
            return data ? curr_format(data*1) : 'N/A';
        }
      },
      {data: 'PROPERTY_VALUE', render: function (data, type, full, meta) {
            return data ? curr_format(data*1) : 'N/A';
        }
      },
      { data: 'USER_STATUS' },
      {data: 'DATE_OF_VAL', render: function (data, type, full, meta) {
            return data ? moment(data, 'YYYY-MM-DD').format('D-MMM-YYYY') : 'N/A';
        }
      },
      { data: 'VALUER_ID', render:function ( data, type, full, meta ) { 
         return "<a href='<?php echo site_url("staff/view"); ?>/" + data + "'>( "+full.valuer_initials+" )</a>";}},
      {data: 'STATUS_NAME', render: function (data, type, full, meta) {
            return "<span class='badge badge-success'>"+data+"</span>";
        }
      }
      <?php if(($_SESSION['ROLE_ID']==1) ||($_SESSION['ROLE_ID']==5)||($_SESSION['ROLE_ID']==3)) { ?>
      ,
       {data: 'created_firstname',
          render: function(data, type, full, meta) {
            return  data + " " + full.created_lastname + "  " + full.created_othernames ;
          }
      },
      {data: 'DATE_CREATED', render: function (data, type, full, meta) {
            return data ? moment(data, 'YYYY-MM-DD').format('D-MMM-YYYY') : 'N/A';
        }
      },
      { data: 'id', render:function ( data, type, full, meta ) {
        var ret_txt ="<a class='badge badge-info edit_me' data-toggle='modal' style='margin-right:10px;' href='#add_property-modal'><i class='fa fa-edit'></i></a>";
        ret_txt += "<a href='#' data-toggle='modal' style='margin-right:10px;' class='badge badge-warning deactivate'><i class='fa fa-refresh'></i></a>";
        <?php if(($_SESSION['ROLE_ID']==1) ||($_SESSION['ROLE_ID']==5)) { ?>
        ret_txt += "<a href='#' data-toggle='modal'  class='badge badge-danger delete_me'><i class='fa fa-trash'></i></a>";
        <?php } ?>
        return ret_txt;
      } }
    <?php } ?>

      ]
      
    });
}
}
