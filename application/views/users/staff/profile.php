<div id="profile" class="tab-pane active" >
    <!-- ================== START YOUR CONTENT HERE =============== -->
    <div class="col-md-12">
        <?php $this->load->view('users/staff/profile_pic_modal.php'); ?>
        <div class="profile-info">
            <div class="row">
                <div class="col-lg-12">
                    <div class="modal-body">

                        <table class="table table-user-information  table-stripped  m-t-md">
                            <tbody >
                                <tr>
                                    <td><strong>Mobile Number</strong></td>
                                    <td colspan="3" ><?php echo $user['MOBILE_NUMBER']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Email</strong></td>
                                    <td colspan="3"><?php echo $user['EMAIL']; ?></td>
                                </tr>
                                 <tr>
                                    <td><strong>Branch</strong></td>
                                    <td colspan="3"><?php echo $user['BRANCH_NAME']; ?></td>
                                </tr>
                               <tr>
                                    <td><strong>Firm/Organisation</strong></td>
                                    <td colspan="3"><?php echo $user['FIRM_NAME']; ?></td>
                                </tr>
                               
                                <tr>
                                    <td><strong>Role</strong></td>
                                    <td ><?php echo $user['ROLE_NAME']; ?></td>
                                    <td><strong>Status</strong></td>
                                    <td ><span class="badge badge-primary"><?php echo $user['STATUS_NAME']; ?></span></td> 
                                </tr>
                                
                                <tr>
                                    <td><strong>Comment</strong></td>
                                    <td ><?php echo $user['COMMENT']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                      <?php
                        $this->load->view('users/staff/signup_modal');
                        ?>
                    </div>  
                </div>
            </div>
        </div>

        <hr>
        <div  class="row"  style="font-size:12px;">
           <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <span><strong>Created By :</strong></span>
                    </div>

                    <div class="col-md-8">
                        <span ><?php echo $user['createdby_firstname']." ".$user['createdby_lastname']; ?></span>
                    </div>

                    <div class="col-md-4">
                        <span><strong>Date :</strong></span>
                    </div>

                    <div class="col-md-8">
                        <span ><?php echo date("d-M-Y",strtotime($user['CREATED_AT'])); ?></span>
                    </div>  
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <span><strong>Modified By:</strong></span>
                    </div>

                    <div class="col-md-8">
                        <span ><?php echo $user['modifiedby_firstname']." ".$user['modifiedby_lastname']; ?></span>
                    </div>

                    <div class="col-md-4">
                        <span><strong>Date:</strong></span>
                    </div>

                    <div class="col-md-8">
                        <span ><?php echo date("d-M-Y",strtotime($user['DATE_MODIFIED'])); ?></span>
                    </div>  
                </div>
            </div>
        </div>
    </div> 

</div>
