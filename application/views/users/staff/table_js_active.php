if ($("#tblStaff").length && tabClicked === "active") {
    if(typeof(dTable['tblStaff'])!=='undefined'){
        $("#inactive").removeClass("active");
        $("#active").addClass("active");
        dTable['tblStaff'].ajax.reload(null,true);
        counter=1;
   }else{
    counter=1;
    dTable['tblStaff'] = $('#tblStaff').DataTable( {
    "pageLength": 10,
    "searching": true,
    "paging": true,
    "processing": false,
    "responsive": true,
    "dom":"<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +"<'row'<'col-sm-12'tr>>" +"<'row'<'col-sm-5'i><'col-sm-7'p>>",
    buttons: <?php if(($_SESSION['ROLE_ID']==1) ||($_SESSION['ROLE_ID']==5)) { ?> getBtnConfig('Staff'), <?php } else { echo "[],"; } ?>
      "ajax": {
        url:"<?php echo site_url("staff/jsonList");?>",
        dataType: 'JSON',
        type: 'POST',
        data: function(d){
          d.status_id=1;
        }
      },
      "order": [[ 0, "desc" ]],
      columnDefs: [{
                  "targets": [5],
                  "orderable": true,
                  "searchable": false
              }],
      columns:[
      {data: 'id', render: function (data, type, full, meta) {
            return counter++;
        }
      },
      { data: 'SALUTATION', render:function ( data, type, full, meta ) { 
         return "<a href='<?php echo site_url("staff/view"); ?>/" + full.id + "'>" +  full.FIRSTNAME +" " + full.LASTNAME + "  " + full.OTHERNAMES + "</a>";}},
      { data: 'INITIALS' },
      { data: 'MOBILE_NUMBER'},
      { data: 'EMAIL'},
      { data: 'BRANCH_NAME' },
      { data: 'STATUS_NAME'}
      <?php if(($_SESSION['ROLE_ID']==1) ||($_SESSION['ROLE_ID']==5)) { ?>
      ,
      { data: 'id', render:function ( data, type, full, meta ) {
        var ret_txt ="<a class='badge badge-info edit_me' data-toggle='modal' style='margin-right:10px;' href='#add_staff-modal'><i class='fa fa-edit'></i></a>";
        ret_txt += "<a href='#' data-toggle='modal' style='margin-right:10px;' class='badge badge-warning deactivate'><i class='fa fa-refresh'></i></a>";
        ret_txt += "<a href='#' data-toggle='modal'  class='badge badge-danger delete_me'><i class='fa fa-trash'></i></a>";

        return ret_txt;
      } }
    <?php } ?>
      ]
      
    });
}
}